# ADMIN SIDEBAR LAYOUT

from py_admin.py_config.py_functions.get_admin_content import get_admin_page_title
from py_admin.py_config.py_functions.get_user_content import get_admin_user_name
from py_admin.py_config.py_functions.get_user_content import get_admin_user_profile_img


def get_admin_sidebar(content_menu=''):
    title = get_admin_page_title()
    fav = get_admin_user_profile_img()
    name = get_admin_user_name()

    r = """<div class="container body">
              <div class="main_container">
                <div class="col-md-3 left_col">
                  <div class="left_col scroll-view">
                  
                    <div class="navbar nav_title" style="border: 0;">
                      <a href="index.py" class="site_title">
                        <i class="fa fa-paw"></i> <span>%s</span>
                      </a>
                    </div>
        
                    <div class="clearfix"></div>
        
                    <!-- menu profile quick info -->
                    <div class="profile clearfix">
                      <div class="profile_pic">
                        <img src=" %s " 
                        alt=" %s " class="img-circle profile_img">
                      </div>
                      <div class="profile_info">
                        <span>Bem Vindo(a),</span>
                        <h2>%s</h2>
                      </div>
                    </div>
                    <!-- /menu profile quick info -->
        
                    <br />
                    
                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                      <!-- menu section -->
                      %s
                      <!-- /menu section -->
                    </div>
                    <!-- /sidebar menu -->
                    
                  </div>
                </div>""" % (title, fav, name, name, content_menu)

    return r
