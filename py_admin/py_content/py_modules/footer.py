# ADMIN FOOTER LAYOUT


from py_admin.py_config.py_functions.get_admin_content import get_admin_copy


def get_admin_footer(path="../"):
    copy = get_admin_copy()
    r = f"""<!-- footer content -->
                <footer>
                  <div class="pull-right">
                    {copy}
                  </div>
                  <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->
              </div>
            </div>
        
            <!-- jQuery -->
            <script src="{path}py_admin/py_content/vendors/jquery/dist/jquery.min.js"></script>
            <!-- Bootstrap -->
            <script src="{path}py_admin/py_content/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
            <!-- FastClick -->
            <script src="{path}py_admin/py_content/vendors/fastclick/lib/fastclick.js"></script>
            <!-- NProgress -->
            <script src="{path}py_admin/py_content/vendors/nprogress/nprogress.js"></script>
            <!-- Chart.js -->
            <script src="{path}py_admin/py_content/vendors/Chart.js/dist/Chart.min.js"></script>
            <!-- gauge.js -->
            <script src="{path}py_admin/py_content/vendors/gauge.js/dist/gauge.min.js"></script>
            <!-- bootstrap-progressbar -->
            <script src="{path}py_admin/py_content/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
            <!-- iCheck -->
            <script src="{path}py_admin/py_content/vendors/iCheck/icheck.min.js"></script>
            <!-- Skycons -->
            <script src="{path}py_admin/py_content/vendors/skycons/skycons.js"></script>
            <!-- Flot -->
            <script src="{path}py_admin/py_content/vendors/Flot/jquery.flot.js"></script>
            <script src="{path}py_admin/py_content/vendors/Flot/jquery.flot.pie.js"></script>
            <script src="{path}py_admin/py_content/vendors/Flot/jquery.flot.time.js"></script>
            <script src="{path}py_admin/py_content/vendors/Flot/jquery.flot.stack.js"></script>
            <script src="{path}py_admin/py_content/vendors/Flot/jquery.flot.resize.js"></script>
            <!-- Flot plugins -->
            <script src="{path}py_admin/py_content/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
            <script src="{path}py_admin/py_content/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
            <script src="{path}py_admin/py_content/vendors/flot.curvedlines/curvedLines.js"></script>
            <!-- DateJS -->
            <script src="{path}py_admin/py_content/vendors/DateJS/build/date.js"></script>
            <!-- JQVMap -->
            <script src="{path}py_admin/py_content/vendors/jqvmap/dist/jquery.vmap.js"></script>
            <script src="{path}py_admin/py_content/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
            <script src="{path}py_admin/py_content/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
            <!-- bootstrap-daterangepicker -->
            <script src="{path}py_admin/py_content/vendors/moment/min/moment.min.js"></script>
            <script src="{path}py_admin/py_content/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
        
            <!-- Custom Theme Scripts -->
            <script src="{path}py_admin/py_content/js/custom.min.js"></script>"""

    return r
