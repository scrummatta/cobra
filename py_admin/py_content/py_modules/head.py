# ADMIN HEAD LAYOUT

from py_admin.py_config.py_functions.get_admin_content import get_admin_page_title
from py_admin.py_config.py_functions.get_admin_content import get_admin_page_fav


def get_admin_head(page="Panel", path="../"):
    fav = get_admin_page_fav()
    title = get_admin_page_title()

    r = f"""<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <!-- Meta, title, CSS, favicons, etc. -->
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            
            <link rel="icon" href="{fav}" type="image/ico" /> 
            
            <title>{title} | {page}</title> 
            
            <!-- Bootstrap -->
            <link href="{path}py_admin/py_content/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
            <!-- Font Awesome -->
            <link href="{path}py_admin/py_content/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
            <!-- NProgress -->
            <link href="{path}py_admin/py_content/vendors/nprogress/nprogress.css" rel="stylesheet">
            <!-- iCheck -->
            <link href="{path}py_admin/py_content/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
            
            <!-- bootstrap-progressbar -->
            <link 
            href="{path}py_admin/py_content/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" 
            rel="stylesheet">
            <!-- JQVMap -->
            <link href="{path}py_admin/py_content/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
            <!-- bootstrap-daterangepicker -->
            <link href="{path}py_admin/py_content/vendors/bootstrap-daterangepicker/daterangepicker.css" 
            rel="stylesheet">
        
            <!-- Custom Theme Style -->
            <link href="{path}py_admin/py_content/css/custom.min.css" rel="stylesheet">"""

    return r
