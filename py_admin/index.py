#!C:/Users/magalu/AppData/Local/Programs/Python/Python36/python.exe
# -*- coding: UTF-8 -*-

import sys
sys.path.append("..")

import py_content.py_modules.head as h
import py_content.py_modules.footer as f
import py_content.py_modules.sidebar as s
import py_config.py_functions.interface as i
import py_config.functions as func

print("Content-type:text/html; ISO-8859-1")
print()

html = """<html lang='pt-br'>
            <head>
                %s
            </head>
            <body>
                %s
                %s
            </body>
        </html>"""

hd = h.get_admin_head()

bd = ""

ft = f.get_admin_footer()

print(html % (hd, bd, ft))
