# DEFAULT FUNCTIONS OF PANEL AND SYSTEM

import sys
import hashlib

# USE TO EXECUTE A FUNCTION WITH SHORTCODES


def short_code_interpreter(func_name, *arg):
    func = getattr(sys.modules[__name__], func_name)
    return func(*arg)


# /SHORTCODES


def generate_pass(password):
    r = hashlib.md5(password.encode('utf-8')).hexdigest()
    return r
