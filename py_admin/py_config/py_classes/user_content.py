# USER CONTENT CLASS

from py_admin.py_config.py_classes.conn import DB


class UserData:
    def __init__(self):
        self.oConexao = DB()

    def admin_user_name(self, user_id):
        sql = f"SELECT user_name FROM py_users WHERE user_id = '{user_id}'"

        self.oConexao.conn.execute(sql)

        r = self.oConexao.conn.fetchall()

        for i in r:
            r = i[0]

        self.oConexao.Close()

        return r

    def admin_user_img(self, user_id):
        sql = f"SELECT user_fav FROM py_users WHERE user_id = '{user_id}'"

        self.oConexao.conn.execute(sql)

        r = self.oConexao.conn.fetchall()

        for i in r:
            r = i[0]

        self.oConexao.Close()

        return r


class UserLogin:
    def __init__(self):
        self.oConexao = DB()

    def check_password(self, user_login, user_pass):
        sql = f"SELECT user_id FROM py_users WHERE user_login = '{user_login}' AND user_pass = '{user_pass}'"

        self.oConexao.conn.execute(sql)

        r = self.oConexao.conn.fetchall()

        for i in r:
            r = i[0]

        self.oConexao.Close()

        return r
