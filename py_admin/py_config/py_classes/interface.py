# VISUAL INTERFACE OBJECTS

from py_admin.py_config.py_functions import interface

# BADGES


class BadgesLabel:
    def __init__(self, label, color='default'):
        self.label = label
        self.color = color
        self.layout = interface.badges(self.label, self.color)

    def get_layout(self):
        self.layout = interface.badges(self.label, self.color)

        return interface.badges_label(self.label, self.color)


class Badges:
    def __init__(self, label, color='default'):
        self.label = label
        self.color = color
        self.layout = interface.badges(self.label, self.color)

    def get_layout(self):
        self.layout = interface.badges(self.label, self.color)

        return self.layout


# /BADGES
# FORMS STRUCTURES


class ResizeTextArea:
    def __init__(self, label, placeholder):
        self.label = label
        self.placeholder = placeholder
        self.layout = interface.resize_text_area(self.layout, self.placeholder)

    def get_layout(self):
        self.layout = interface.resize_text_area(self.layout, self.placeholder)

        return self.layout


class FormButtonCancel:
    def __init__(self, label, color='default'):
        self.label = label
        self.color = color
        self.layout = interface.form_button_cancel(self.label, self.color)

    def get_layout(self):
        self.layout = interface.form_button_cancel(self.label, self.color)

        return self.layout


class FormButtonReset:
    def __init__(self, label, color='default'):
        self.label = label
        self.color = color
        self.layout = interface.form_button_reset(self.label, self.color)

    def get_layout(self):
        self.layout = interface.form_button_reset(self.label, self.color)

        return self.layout


class FormButtonSubmit:
    def __init__(self, label, color='default'):
        self.label = label
        self.color = color
        self.layout = interface.form_button_submit(self.label, self.color)

    def get_layout(self):
        self.layout = interface.form_button_submit(self.label, self.color)

        return self.layout


class InputHorizontal:
    def __init__(self, label, placeholder, input_id, input_type='text', required=False, input_addon=''):
        self.label = label
        self.placeholder = placeholder
        self.input_id = input_id
        self.input_type = input_type
        self. required = required
        self.input_addon = input_addon
        self.layout = interface.input_horizontal(self.label, self.placeholder, self.input_id,
                                                 self.input_type, self.required, self.input_addon)

    def get_layout(self):
        self.layout = interface.input_horizontal(self.label, self.placeholder, self.input_id,
                                                 self.input_type, self.required, self.input_addon)

        return self.layout


class InputVertical:
    def __init__(self, label, placeholder, input_id, input_type='text', required=False, input_addon=''):
        self.label = label
        self.placeholder = placeholder
        self.input_id = input_id
        self.input_type = input_type
        self.required = required
        self.input_addon = input_addon
        self.layout = interface.input_vertical(self.label, self.placeholder, self.input_id,
                                               self.input_type, self.required, self.input_addon)

    def get_layout(self):
        self.layout = interface.input_vertical(self.label, self.placeholder, self.input_id,
                                               self.input_type, self.required, self.input_addon)

        return self.layout


class InputIcon:
    def __init__(self, placeholder, input_id, input_type='text', required=False, input_float='left', input_addon=''):
        self.placeholder = placeholder
        self.input_id = input_id
        self.input_type = input_type
        self.required = required
        self.input_float = input_float
        self.input_addon = input_addon
        self.layout = interface.input_icon(self.placeholder, self.input_id, self.input_type,
                                           self.required, self.input_float, self.input_addon)

    def get_layout(self):
        self.layout = interface.input_icon(self.placeholder, self.input_id, self.input_type,
                                           self.required, self.input_float, self.input_addon)

        return self.layout


class InputToggle:
    def __init__(self, title, input_id, label, color='default', input_addon=''):
        self.title = title
        self.input_id = input_id
        self.label = label
        self.color = color
        self.input_addon = input_addon
        self.layout = interface.input_toggle(self.title, self.input_id, self.label, self.color, self.input_addon)

    def get_layout(self):
        self.layout = interface.input_toggle(self.title, self.input_id, self.label, self.color, self.input_addon)

        return self.layout


class InputSelect:
    def __init__(self, label, options, input_addon=''):
        self.label = label
        self.options = options
        self.input_addon = input_addon
        self.layout = interface.input_select(self.label, self.options, self.input_addon)

    def get_layout(self):
        self.layout = interface.input_select(self.label, self.options, self.input_addon)

        return self.layout


class InputSelectGrouped:
    def __init__(self, label, options, input_addon=''):
        self.label = label
        self.options = options
        self.input_addon = input_addon
        self.layout = interface.input_select_grouped(self.label, self.options, self.input_addon)

    def get_layout(self):
        self.layout = interface.input_select_grouped(self.label, self.options, self.input_addon)

        return self.layout


class InputCheckBox:
    def __init__(self, label, input_addon=''):
        self.label = label
        self.input_addon = input_addon
        self.layout = interface.input_check_box(self.label, self.input_addon)

    def get_layout(self):
        self.layout = interface.input_check_box(self.label, self.input_addon)

        return self.layout


class InputCheckRadio:
    def __init__(self, label, name, input_addon=''):
        self.label = label
        self.name = name
        self.input_addon = input_addon
        self.layout = interface.input_check_radio(self.label, self.name, self.input_addon)

    def get_layout(self):
        self.layout = interface.input_check_radio(self.label, self.name, self.input_addon)

        return self.layout


class InputSwitch:
    def __init__(self, label, input_addon=''):
        self.label = label
        self.input_addon = input_addon
        self.layout = interface.input_switch(self.label, self.input_addon)

    def get_layout(self):
        self.layout = interface.input_switch(self.label, self.input_addon)

        return self.layout


class InputColor:
    def __init__(self, label, value='#ffffff'):
        self.label = label
        self.value = value
        self.layout = interface.input_color(self.label, self.value)

    def get_layout(self):
        self.layout = interface.input_color(self.label, self.value)

        return self.layout


class InputDatePicker:
    def __init__(self, label, input_id):
        self.label = label
        self.input_id = input_id
        self.layout = interface.input_date_picker(self.label, self.input_id)

    def get_layout(self):
        self.layout = interface.input_date_picker(self.label, self.input_id)

        return self.layout


# /FORMS STRUCTURES
# BOXERS


class FloatAlertBox:
    def __init__(self, title, alert, icon='fa-info-circle', color='dark'):
        self.title = title
        self.alert = alert
        self.icon = icon
        self.color = color
        self.layout = interface.float_alert_box(self.title, self.alert, self.icon, self.color)

    def get_layout(self):
        self.layout = interface.float_alert_box(self.title, self.alert, self.icon, self.color)

        return self.layout


class AlertBox:
    def __init__(self, alert_strong, alert, color='dark'):
        self.alert_strong = alert_strong
        self.alert = alert
        self.color = color
        self.layout = interface.alert_box(self.alert_strong, self.alert, self.color)

    def get_layout(self):
        self.layout = interface.alert_box(self.alert_strong, self.alert, self.color)

        return self.layout


class FormBox:
    def __init__(self, title, content, buttons, form_id):
        self.title = title
        self.content = content
        self.buttons = buttons
        self.form_id = form_id
        self.layout = interface.form_box(self.title, self.content, self.buttons, self.form_id)

    def get_layout(self):
        self.layout = interface.form_box(self.title, self.content, self.buttons, self.form_id)

        return self.layout


class ContentPanel:
    def __init__(self, title, content):
        self.title = title
        self.content = content
        self.layout = interface.content_panel(self.title, self.content)

    def get_layout(self):
        self.layout = interface.content_panel(self.title, self.content)

        return self.layout


# //BOXERS
# BUTTONS


class ButtonCommon:
    def __init__(self, label, button_addon='', size='default', color='default'):
        self.label = label
        self.button_addon = button_addon
        self.size = size
        self.color = color
        self.layout = interface.button_common(self.label, self.button_addon, self.size, self.color)

    def get_layout(self):
        self.layout = interface.button_common(self.label, self.button_addon, self.size, self.color)

        return self.layout


class ButtonRounded:
    def __init__(self, label, button_addon='', size='default', color='default'):
        self.label = label
        self.button_addon = button_addon
        self.size = size
        self.color = color
        self.layout = interface.button_rounded(self.label, self.button_addon, self.size, self.color)

    def get_layout(self):
        self.layout = interface.button_rounded(self.label, self.button_addon, self.size, self.color)

        return self.layout


class ButtonDropdown:
    def __init__(self, label, dropitens, separated, color='default'):
        self.label = label
        self.dropitens = dropitens
        self.separated = separated
        self.color = color
        self.layout = interface.button_dropdown(self.label, self.dropitens, self.separated, self.color)

    def get_layout(self):
        self.layout = interface.button_dropdown(self.label, self.dropitens, self.separated, self.color)

        return self.layout


class ButtonApp:
    def __init__(self, icon, value):
        self.icon = icon
        self.value = value
        self.layout = interface.button_app(self.icon, self.value)

    def get_layout(self):
        self.layout = interface.button_app(self.icon, self.value)

        return self.layout


class ButtonPaginator:
    def __init__(self, start, end, current, color='default'):
        self.start = start
        self.end = end
        self.current = current
        self.color = color
        self.layout = interface.button_paginator(self.start, self.end, self.current, self.color)

    def get_layout(self):
        self.layout = interface.button_paginator(self.start, self.end, self.current, self.color)

        return self.layout


# //BUTTONS
# OTHER LAYOUTS


class SideMenuSection:
    def __init__(self, title, lists):
        self.title = title
        self.lists = lists
        self.layout = interface.side_menu_section(self.title, self.lists)

    def get_layout(self):
        self.layout = interface.side_menu_section(self.title, self.lists)

        return self.layout


class SideMenuChild:
    def __init__(self, title, icon, child):
        self.title = title
        self.icon = icon
        self.child = child
        self.layout = interface.side_menu_child(self.title, self.icon, self.child)

    def get_layout(self):
        self.layout = interface.side_menu_child(self.title, self.icon, self.child)

        return self.layout


class MediaThumb:
    def __init__(self, img_url, img_id, caption=''):
        self.img_url = img_url
        self.img_id = img_id
        self.caption = caption
        self.layout = interface.media_thumb(self.img_url, self.img_id, self.caption)

    def get_layout(self):
        self.layout = interface.media_thumb(self.img_url, self.img_id, self.caption)

        return self.layout


class Table:
    def __init__(self, data, content):
        self.data = data
        self.content = content
        self.layout = interface.table(self.data, self.content)

    def get_layout(self):
        self.layout = interface.table(self.data, self.content)

        return self.layout


class HorizontalTabGroup:
    def __init__(self, tabs):
        self.tabs = tabs
        self.layout = interface.horizontal_tab_group(self.tabs)

    def get_layout(self):
        self.layout = interface.horizontal_tab_group(self.tabs)

        return self.layout


class VerticalTabGroup:
    def __init__(self, tabs):
        self.tabs = tabs
        self.layout = interface.horizontal_tab_group(self.tabs)

    def get_layout(self):
        self.layout = interface.horizontal_tab_group(self.tabs)

        return self.layout


class AccordionGroup:
    def __init__(self, accordion):
        self.accordion = accordion
        self.layout = interface.accordion_group(self.accordion)

    def get_layout(self):
        self.layout = interface.accordion_group(self.accordion)

        return self.layout


class ModalSmall:
    def __init__(self, title, content, modal_class):
        self.title = title
        self.content = content
        self.modal_class = modal_class
        self.layout = interface.modal_small(self.title, self.content, self.modal_class)

    def get_layout(self):
        self.layout = interface.modal_small(self.title, self.content, self.modal_class)

        return self.layout


class ModalLarge:
    def __init__(self, title, content, modal_class):
        self.title = title
        self.content = content
        self.modal_class = modal_class
        self.layout = interface.modal_large(self.title, self.content, self.modal_class)

    def get_layout(self):
        self.layout = interface.modal_large(self.title, self.content, self.modal_class)

        return self.layout


class CheckboxNotification:
    def __init__(self, label, is_checked=''):
        self.label = label
        self.is_checked = is_checked
        self.layout = interface.checkbox_notification(self.label, self.is_checked)

    def get_layout(self):
        self.layout = interface.checkbox_notification(self.label, self.is_checked)

        return self.layout


class ImageNotification:
    def __init__(self, title, information, mensage, img_url, notification_url):
        self.title = title
        self.information = information
        self.mensage = mensage
        self.img_url = img_url
        self.notification_url = notification_url
        self.layout = interface.image_notification(self.title, self.information, self.mensage,
                                                   self.img_url, self.notification_url)

    def get_layout(self):
        self.layout = interface.image_notification(self.title, self.information, self.mensage,
                                                   self.img_url, self.notification_url)

        return self.layout


class HorizontalProgressBar:
    def __init__(self, progress='0', color='info'):
        self.progress = progress
        self.color = color
        self.layout = interface.horizontal_progress_bar(self.progress, self.color)

    def get_layout(self):
        self.layout = interface.horizontal_progress_bar(self.progress, self.color)

        return self.layout


class VerticalProgressBar:
    def __init__(self, progress='0', color='info'):
        self.progress = progress
        self.color = color
        self.layout = interface.vertical_progress_bar(self.progress, self.color)

    def get_layout(self):
        self.layout = interface.vertical_progress_bar(self.progress, self.color)

        return self.layout


class InputEditor:
    def __init__(self, label, placeholder, input_id, span=''):
        self.label = label
        self.placeholder = placeholder
        self.input_id = input_id
        self.span = span
        self.layout = interface.input_editor(self.label, self.placeholder, self.input_id, self.span)

    def get_layout(self):
        self.layout = interface.input_editor(self.label, self.placeholder, self.input_id, self.span)

        return self.layout


class ButtonEditor:
    def __init__(self, label, icon, method, option, color='default'):
        self.label = label
        self.icon = icon
        self.method = method
        self.option = option
        self.color = color
        self.layout = interface.button_editor(self.label, self.icon, self.method, self.option, self.color)

    def get_layout(self):
        self.layout = interface.button_editor(self.label, self.icon, self.method, self.option, self.color)

        return self.layout


class LabelEditor:
    def __init__(self, label, title, value, input_id):
        self.label = label
        self.title = title
        self.value = value
        self.input_id = input_id
        self.layout = interface.label_editor(self.label, self.title, self.value, self.input_id)

    def get_layout(self):
        self.layout = interface.label_editor(self.label, self.title, self.value, self.input_id)

        return self.layout


class ImageEditor:
    def __init__(self, img_url):
        self.img_url = img_url
        self.layout = interface.image_editor(self.img_url)

    def get_layout(self):
        self.layout = interface.image_editor(self.img_url)

        return self.layout


class DropZone:
    def __init__(self, title, notice=''):
        self.title = title
        self.notice = notice
        self.layout = interface.drop_zone(self.title, self.notice)

    def get_layout(self):
        self.layout = interface.drop_zone(self.title, self.notice)

        return self.layout


# /OTHER LAYOUTS
