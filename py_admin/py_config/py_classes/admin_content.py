# ADMIN CONTENT CLASS

from py_admin.py_config.py_classes.conn import DB


class PanelData:
    def __init__(self):
        self.oConexao = DB()

    def page_title(self):
        sql = "select option_value from py_options where option_name = 'admin_page_title'"

        self.oConexao.conn.execute(sql)

        r = self.oConexao.conn.fetchall()

        for i in r:
            r = i[0]

        self.oConexao.Close()

        return r

    def page_fav(self):
        sql = "SELECT option_value FROM py_options WHERE option_name = 'admin_page_fav'"

        self.oConexao.conn.execute(sql)

        r = self.oConexao.conn.fetchall()

        for i in r:
            r = i[0]

        self.oConexao.Close()

        return r

    def copy(self):
        sql = "SELECT option_value FROM py_options WHERE option_name = 'admin_copy'"

        self.oConexao.conn.execute(sql)

        r = self.oConexao.conn.fetchall()

        for i in r:
            r = i[0]

        self.oConexao.Close()

        return r

