# POST CLASSES

from py_admin.py_config.py_classes.conn import DB


class SinglePost:
    def __init__(self, post_id):
        self.oConexao = DB()
        self.id = post_id

    def get_title(self):
        sql = "select post_title from py_posts where id = %s" % self.id

        self.oConexao.conn.execute(sql)

        r = self.oConexao.conn.fetchall()

        for i in r:
            r = i[0]

        self.oConexao.Close()

        return r

    def get_parent(self):
        sql = "select post_parent from py_posts where id = %s" % self.id

        self.oConexao.conn.execute(sql)

        r = self.oConexao.conn.fetchall()

        for i in r:
            r = i[0]

        self.oConexao.Close()

        return r

    def get_author(self):
        sql = "select post_author from py_posts where id = %s" % self.id

        self.oConexao.conn.execute(sql)

        r = self.oConexao.conn.fetchall()

        for i in r:
            r = i[0]

        self.oConexao.Close()

        return r

    def get_date(self, is_gmt):
        if is_gmt:
            sql = "select post_date_gmt from py_posts where id = %s" % self.id

            self.oConexao.conn.execute(sql)

            r = self.oConexao.conn.fetchall()

        else:
            sql = "select post_date from py_posts where id = %s" % self.id

            self.oConexao.conn.execute(sql)

            r = self.oConexao.conn.fetchall()

        for i in r:
            r = i[0]

        self.oConexao.Close()

        return r

    def get_modified_date(self):
        sql = "select post_modified from py_posts where id = %s" % self.id

        self.oConexao.conn.execute(sql)

        r = self.oConexao.conn.fetchall()

        for i in r:
            r = i[0]

        self.oConexao.Close()

        return r

    def get_content(self):
        sql = "select post_content from py_posts where id = %s" % self.id

        self.oConexao.conn.execute(sql)

        r = self.oConexao.conn.fetchall()

        for i in r:
            r = i[0]

        self.oConexao.Close()

        return r

    def get_status(self):
        sql = "select post_status from py_posts where id = %s" % self.id

        self.oConexao.conn.execute(sql)

        r = self.oConexao.conn.fetchall()

        for i in r:
            r = i[0]

        self.oConexao.Close()

        return r

    def get_guid(self):
        sql = "select guid from py_posts where id = %s" % self.id

        self.oConexao.conn.execute(sql)

        r = self.oConexao.conn.fetchall()

        for i in r:
            r = i[0]

        self.oConexao.Close()

        return r

    def get_type(self):
        sql = "select post_type from py_posts where id = %s" % self.id

        self.oConexao.conn.execute(sql)

        r = self.oConexao.conn.fetchall()

        for i in r:
            r = i[0]

        self.oConexao.Close()

        return r

    def get_mime_type(self):
        sql = "select post_mime_type from py_posts where id = %s" % self.id

        self.oConexao.conn.execute(sql)

        r = self.oConexao.conn.fetchall()

        for i in r:
            r = i[0]

        self.oConexao.Close()

        return r

    def get_menu_order(self):
        sql = "select menu_order from py_posts where id = %s" % self.id

        self.oConexao.conn.execute(sql)

        r = self.oConexao.conn.fetchall()

        for i in r:
            r = i[0]

        self.oConexao.Close()

        return r


class PostMeta:
    def __init__(self, post_id):
        self.oConexao = DB()
        self.id = post_id

    def get_id(self, value):
        sql = "select id from py_postmeta where post_id = %s and meta_key = %s" % (self.id, value)

        self.oConexao.conn.execute(sql)

        r = self.oConexao.conn.fetchall()

        for i in r:
            r = i[0]

        self.oConexao.Close()

        return r

    def get_meta_value(self, value):
        sql = "select meta_value from py_postmeta where post_id = %s and meta_key = %s" % (self.id, value)

        self.oConexao.conn.execute(sql)

        r = self.oConexao.conn.fetchall()

        for i in r:
            r = i[0]

        self.oConexao.Close()

        return r
