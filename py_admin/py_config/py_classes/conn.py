import pymysql as MySQLdb


class DB:

    def __init__(self):
        self.bd = MySQLdb.connect(host="localhost", user="root", passwd="", db="scrumm_db")

        self.conn = self.bd.cursor()

    def Close(self):
        self.conn.close()