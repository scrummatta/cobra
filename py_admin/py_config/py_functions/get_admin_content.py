# ADMIN CONTENT LAYOUT

from py_admin.py_config.py_classes.admin_content import PanelData


def get_admin_page_title():
    d = PanelData()
    r = d.page_title()

    return r


def get_admin_page_fav():
    d = PanelData()
    r = d.page_fav()

    return r


def get_admin_copy():
    d = PanelData()
    r = d.copy()

    return r





