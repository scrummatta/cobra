#!C:/Users/magalu/AppData/Local/Programs/Python/Python36/python.exe

import cgi
from py_admin.py_config.py_functions.get_user_content import login_verify

print("Content-type: text/html\n")
print()
html = """<html>
            <body>
                %s
            </body>
        </html>"""

storage = cgi.FieldStorage()

username = storage.getvalue('username')
password = storage.getvalue('password')

r = login_verify(username, password)
h = f"<h1>Hello {r}, {username}</h1>"

print(html % h)
