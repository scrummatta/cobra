# VISUAL INTERFACE GENERATORS

# BADGES


def badges_label(label, color='default'):
    # types: primary success info warning danger dark link
    # you can create you colors and works here

    r = """<span class="label label-""" + color + """\">""" + label + """</span>"""

    return r


def badges(label, color='red'):
    # use color name to set the color
    # you can create you colors and works here

    r = """<span class="badge bg-""" + color + """\">""" + label + """</span>"""

    return r


# /BADGES
# FORMS STRUCTURES


def text_area():

    r = """<div id="alerts"></div>
          <div class="btn-toolbar editor" data-role="editor-toolbar" data-target="#editor-one">
            <div class="btn-group">
              <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font">
                <i class="fa fa-font"></i><b class="caret"></b>
              </a>
              <ul class="dropdown-menu">
              </ul>
            </div>

            <div class="btn-group">
              <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font Size">
                <i class="fa fa-text-height"></i>&nbsp;<b class="caret"></b>
              </a>
              <ul class="dropdown-menu">
                <li>
                  <a data-edit="fontSize 5">
                    <p style="font-size:17px">Huge</p>
                  </a>
                </li>
                <li>
                  <a data-edit="fontSize 3">
                    <p style="font-size:14px">Normal</p>
                  </a>
                </li>
                <li>
                  <a data-edit="fontSize 1">
                    <p style="font-size:11px">Small</p>
                  </a>
                </li>
              </ul>
            </div>

            <div class="btn-group">
              <a class="btn" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="fa fa-bold"></i></a>
              <a class="btn" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i class="fa fa-italic"></i></a>
              <a class="btn" data-edit="strikethrough" title="Strikethrough"><i class="fa fa-strikethrough"></i></a>
              <a class="btn" data-edit="underline" title="Underline (Ctrl/Cmd+U)"><i class="fa fa-underline"></i></a>
            </div>

            <div class="btn-group">
              <a class="btn" data-edit="insertunorderedlist" title="Bullet list"><i class="fa fa-list-ul"></i></a>
              <a class="btn" data-edit="insertorderedlist" title="Number list"><i class="fa fa-list-ol"></i></a>
              <a class="btn" data-edit="outdent" title="Reduce indent (Shift+Tab)"><i class="fa fa-dedent"></i></a>
              <a class="btn" data-edit="indent" title="Indent (Tab)"><i class="fa fa-indent"></i></a>
            </div>

            <div class="btn-group">
              <a class="btn" data-edit="justifyleft" title="Align Left (Ctrl/Cmd+L)">
                <i class="fa fa-align-left"></i>
              </a>
              <a class="btn" data-edit="justifycenter" title="Center (Ctrl/Cmd+E)">
                <i class="fa fa-align-center"></i>
              </a>
              <a class="btn" data-edit="justifyright" title="Align Right (Ctrl/Cmd+R)">
                <i class="fa fa-align-right"></i>
              </a>
              <a class="btn" data-edit="justifyfull" title="Justify (Ctrl/Cmd+J)">
                <i class="fa fa-align-justify"></i>
              </a>
            </div>

            <div class="btn-group">
              <a class="btn dropdown-toggle" data-toggle="dropdown" title="Hyperlink"><i class="fa fa-link"></i></a>
              <div class="dropdown-menu input-append">
                <input class="span2" placeholder="URL" type="text" data-edit="createLink" />
                <button class="btn" type="button">Add</button>
              </div>
              <a class="btn" data-edit="unlink" title="Remove Hyperlink"><i class="fa fa-cut"></i></a>
            </div>

            <div class="btn-group">
              <a class="btn" title="Insert picture (or just drag & drop)" id="pictureBtn">
                <i class="fa fa-picture-o"></i>
              </a>
              <input type="file" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage" />
            </div>

            <div class="btn-group">
              <a class="btn" data-edit="undo" title="Undo (Ctrl/Cmd+Z)"><i class="fa fa-undo"></i></a>
              <a class="btn" data-edit="redo" title="Redo (Ctrl/Cmd+Y)"><i class="fa fa-repeat"></i></a>
            </div>
          </div>
          <div id="editor-one" class="editor-wrapper"></div>
          <textarea name="descr" id="descr" style="display:none;"></textarea>"""

    return r


def resize_text_area(label, placeholder):
    r = """<div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">""" + label + """</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
              <textarea class="resizable_textarea form-control" placeholder=\"""" + placeholder + """\"></textarea>
            </div>
          </div>"""
    return r


def form_button_cancel(label, color='default'):
    # types: primary success info warning danger dark link
    # you can create you colors and works here

    r = """<button class="btn btn-""" + color + """\" type="button">""" + label + """</button>"""

    return r


def form_button_reset(label, color='default'):
    # types: primary success info warning danger dark link
    # you can create you colors and works here

    r = """<button class="btn btn-""" + color + """\" type="reset">""" + label + """</button>"""

    return r


def form_button_submit(label, color='default'):
    # types: primary success info warning danger dark link
    # you can create you colors and works here

    r = """<button type="submit" class="btn btn-""" + color + """\">""" + label + """</button>"""

    return r


def input_horizontal(label, placeholder, input_id, input_type='text', required=False, input_addon=''):
    # send True to required to show a '*' and make this field required.
    # placeholder send a empty string if not to show
    # example to input_addon disabled="disabled" or readonly="readonly"

    r = """<div class="form-group">"""

    if required:
        r += """<label class="control-label col-md-3 col-sm-3 col-xs-12" for=\"""" + input_id + """\">
                """ + label + """ <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type=\"""" + input_type + """\" id=\"""" + input_id + """\" required="required" 
                    placeholder=\"""" + placeholder + """\" 
                    class="form-control col-md-7 col-xs-12\" """ + input_addon + """>"""
    else:
        r += """<label class="control-label col-md-3 col-sm-3 col-xs-12" for=\"""" + input_id + """\">
                """ + label + """</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type=\"""" + input_type + """\" id=\"""" + input_id + """\" placeholder=\"
                    """ + placeholder + """\" class="form-control col-md-7 col-xs-12\" """ + input_addon + """>"""
    r += """</div>
            </div>"""

    return r


def input_vertical(label, placeholder, input_id, input_type='text', required=False, input_addon=''):
    # send True to required to show a '*' and make this field required.
    # placeholder send a empty string if not to show
    # example to input_addon disabled="disabled" or readonly="readonly"
    # data-inputmask="'mask' : '(999) 999-9999'" on input_addon to generate mask for type of input

    if required:
        r = """<label for=\"""" + input_id + """\">""" + label + """ <span class="required">*</span> :</label>
                <input type=\"""" + input_type + """\" id=\"""" + input_id + """\" class="form-control" placeholder=\"
                    """ + placeholder + """\" name=\"""" + input_id + """\" 
                    required="required\" """ + input_addon + """/>"""
    else:
        r = """<label for=\"""" + input_id + """\">""" + label + """ :</label>
                <input type=\"""" + input_type + """\" id=\"""" + input_id + """\" class="form-control" placeholder=\"
                    """ + placeholder + """\" name=\"""" + input_id + """\" """ + input_addon + """/>"""
    return r


def input_icon(placeholder, input_id, icon, required=False, input_type='text', input_float='left', input_addon=''):
    # send True to required to make this field required.
    # placeholder send a empty string if not to show
    # example to input_addon disabled="disabled" or readonly="readonly"

    r = """<div class="form-group has-feedback">"""

    if required:
        r += """<input type=\"""" + input_type + """\" class="form-control has-feedback-""" + input_float + """\" 
                name=\"""" + input_id + """\" placeholder=\"""" + placeholder + """\" 
                required="required\" """ + input_addon + """>"""
    else:
        r += """<input type=\"""" + input_type + """\" class="form-control has-feedback-""" + input_float + """\" 
                name=\"""" + input_id + """\" placeholder=\"""" + placeholder + """\" """ + input_addon + """>"""

    r += """<span class="fa fa-""" + icon + """ form-control-feedback """ + input_float + """\" aria-hidden="true">
            </span>
            </div>"""

    return r


def input_toggle(title, input_id, label, color='default', input_addon=''):
    # types: primary success info warning danger dark link
    # you can create you colors and works here
    # the value field of the input is generated with the content. Example content = My Home, value=my-home
    # content is a array of label for input
    # example to input_addon disabled="disabled" or readonly="readonly"

    r = """<div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">""" + title + """</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div id=\"""" + input_id + """\" class="btn-group" data-toggle="buttons">"""

    for i in label:
        v = i
        v.lower()
        v.replace(" ", "-")

        r += """<label class="btn btn-default" data-toggle-class="btn-""" + color + """\" 
                data-toggle-passive-class="btn-default">
                    <input type="radio" name=\"""" + input_id + """\" value=\"""" + v + """\" """ + input_addon + """> 
                    &nbsp; """ + i + """ &nbsp;
                </label>"""

    r += """    </div>
            </div>
            </div>"""

    return r


def input_select(label, options, input_addon=''):
    # options is a array of option label
    # in this def input_addon is a array or the same addon value for all options
    # example to input_addon: [value="XYZ", value="ZYX", ...]

    r = """<div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">""" + label + """</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
              <select class="form-control">"""
    j = 0
    for i in options:
        r += """<option """ + input_addon + """>""" + i + """</option>"""
        j += 1

    r += """</select>
            </div>
          </div>"""

    return r


def input_select_grouped(label, options, input_addon=''):
    # options is a two-dimensional array of option label. 0 is the group name
    # in this def input_addon is a array or the same addon value for all options
    # example to input_addon: [value="XYZ", value="ZYX", ...]

    r = """<div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">""" + label + """</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
              <select class="select2_group form-control">"""

    for i in options:
        r += """<optgroup label=\"""" + i[0] + """\">"""

        for j in i[1:]:
            r += """<option """ + input_addon + """>""" + j + """</option>"""

    r += """</select>
            </div>
          </div>"""

    return r


def input_check_box(label, input_addon=''):
    # example to input_addon: disabled="disabled" or readonly="readonly"

    r = """<div class="checkbox">
            <label>
              <input type="checkbox" class="flat\" """ + input_addon + """> """ + label + """
            </label>
          </div>"""

    return r


def input_check_radio(label, name, input_addon=''):
    # name is the same for select one of multiple options
    # example to input_addon: disabled="disabled" or readonly="readonly"

    r = """<div class="radio">
            <label>
              <input type="radio" class="flat" name=\"""" + name + """\" """ + input_addon + """> """ + label + """
            </label>
          </div>"""

    return r


def input_switch(label, input_addon=''):
    r = """<div class="">
            <label>
              <input type="checkbox" class="js-switch\" """ + input_addon + """/> """ + label + """
            </label>
          </div>"""

    return r


def input_color(label, value='#ffffff'):
    # the value need to add a # for hexa color.
    # use value to show a default color.
    r = """<div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">""" + label + """</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
              <div class="input-group demo2">
                <input type="text" value=\"""" + value + """\" class="form-control" />
                <span class="input-group-addon"><i></i></span>
              </div>
            </div>
          </div>"""

    return r


def input_date_picker(label, input_id):
    # use myDatepicker, myDatepicker2 or myDatepicker3 to create a date or hour selector
    # myDatepicker: Basic, hour and date | myDatepicker2: Only date | myDatepicker3: Only hour

    r = """<div class='col-sm-4'>
                    """ + label + """
                    <div class="form-group">
                        <div class='input-group date' id='""" + input_id + """'>
                            <input type='text' class="form-control" />
                            <span class="input-group-addon">
                               <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>"""

    return r


# //FORMS STRUCTURES
# BOXERS


def float_alert_box(title, alert, icon='fa-info-circle', color='dark'):
    # put this box ever on end of page for normaly work of then.
    # types: primary success info warning danger dark link
    # you can create you colors and works here

    r = """<div class="ui-pnotify """ + color + """ ui-pnotify-fade-normal ui-pnotify-move ui-pnotify-in 
            ui-pnotify-fade-in" aria-live="assertive" aria-role="alertdialog" 
            style="display: none; width: 300px; right: 36px; top: 36px;">
              <div class="alert ui-pnotify-container alert-info ui-pnotify-shadow" role="alert" 
              style="min-height: 16px;">
                <div class="ui-pnotify-closer" aria-role="button" tabindex="0" title="Close" style="cursor: pointer; 
                visibility: hidden; display: none;">
                  <span class="glyphicon glyphicon-remove"></span>
                </div>
                <div class="ui-pnotify-sticker" aria-role="button" aria-pressed="true" tabindex="0" title="Unstick" 
                style="cursor: pointer; visibility: hidden; display: none;">
                  <span class="glyphicon glyphicon-play" aria-pressed="true"></span>
                </div>
                <div class="ui-pnotify-icon">
                  <span class="fa """ + icon + """\"></span>
                </div>
                <h4 class="ui-pnotify-title">""" + title + """</h4>
                <div class="ui-pnotify-text" aria-role="alert">
                  """ + alert + """
                </div>
              </div>
            </div>"""

    return r


def alert_box(alert_strong, alert, color='dark'):
    # types: primary success info warning danger dark link
    # you can create you colors and works here

    r = """<div class="alert alert-""" + color + """ alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <strong>""" + alert_strong + """</strong> """ + alert + """
          </div>"""

    return r


def form_box(title, content, buttons, form_id, action="py_admin/py_config/py_functions/forms.py"):
    # content is a html string of form fields
    # buttons is a html string of form buttons(submit, reset, cancel....)

    r = """<div class="x_panel">
              <div class="x_title">
                <h2> """ + title + """ </h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                <br />
                <form id=\"""" + form_id + """\" data-parsley-validate class="form-horizontal 
                form-label-left" method="post" action=\"""" + action + """\">
                  """ + content + """
                  <div class="ln_solid"></div>
                  <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                      """ + buttons + """
                    </div>
                  </div>
                </form>
              </div>
            </div>"""

    return r


def content_panel(title, content):
    # send content like html text to create a content

    r = """
        <div class="x_panel">
        <div class="x_title">
            <h2>""" + title + """</h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">""" + content + """</div>
        </div>"""

    return r


# //BOXERS
# BUTTONS


def button_common(label, button_addon='', size='default', color='default'):
    # types: primary success info warning danger dark link
    # you can create you colors and works here

    if size == 'large':
        r = """<button type="button" class="btn btn-""" + color + """ btn-lg\" """ + button_addon + """>
        """ + label + """</button>"""
    elif size == 'medium':
        r = """<button type="button" class="btn btn-""" + color + """ btn-sm\" """ + button_addon + """>
        """ + label + """</button>"""
    elif size == 'small':
        r = """<button type="button" class="btn btn-""" + color + """ btn-xs\" """ + button_addon + """>
        """ + label + """</button>"""
    else:
        r = """<button type="button" class="btn btn-""" + color + """\" """ + button_addon + """>
        """ + label + """</button>"""

    return r


def button_rounded(label, button_addon='', size='default', color='default'):
    # types: primary success info warning danger dark link
    # you can create you colors and works here

    if size == 'large':
        r = """<button type="button" class="btn btn-round btn-""" + color + """ btn-lg\" """ + button_addon + """>
        """ + label + """</button>"""
    elif size == 'medium':
        r = """<button type="button" class="btn btn-round btn-""" + color + """ btn-sm\" """ + button_addon + """>
        """ + label + """</button>"""
    elif size == 'small':
        r = """<button type="button" class="btn btn-round btn-""" + color + """ btn-xs\" """ + button_addon + """>
        """ + label + """</button>"""
    else:
        r = """<button type="button" class="btn btn-round btn-""" + color + """\" """ + button_addon + """>
        """ + label + """</button>"""

    return r


def button_dropdown(label, dropitens, separated, color='default'):
    # types: primary success info warning danger dark link
    # you can create you colors and works here
    r = """
        <div class="btn-group">
            <button data-toggle="dropdown" class="btn btn-""" + color + """ 
            dropdown-toggle" type="button" aria-expanded="false">
                """ + label + """ <span class="caret"></span>
            </button>
            <ul role="menu" class="dropdown-menu">"""

    for item in dropitens:
        r += """
                <li><a href="#">""" + item + """</a></li>"""

    r += """    <li class="divider"></li>
                <li><a href="#">""" + separated + """</a></li>
              </ul>
        </div>"""

    return r


def button_app(icon, value):
    # use font awesome icons to variable icon
    # for example: fa-edit fa-play fa-save fa-users

    r = """
        <a class="btn btn-app"><i class="fa """ + icon + """\"></i>""" + value + """</a>"""

    return r


def button_paginator(start, end, current, color='default'):
    # types: primary success info warning danger dark link
    # you can create you colors and works here

    r = """<div class="btn-group">"""

    for i in range(start, end):
        if i == current:
            r += """<button class="btn btn-""" + color + """ active" type="button">""" + str(i) + """</button>"""
        else:
            r += """<button class="btn btn-""" + color + """\" type="button">""" + str(i) + """</button>"""

    r += """</div>"""

    return r


# //BUTTONS
# OTHER LAYOUTS


def side_menu_section(title, lists):
    # list is html <li>'s of this menu

    r = """<div class="menu_section">
            <h3>""" + title + """</h3>
            <ul class="nav side-menu">
            """ + lists + """
            </ul>
          </div>"""

    return r


def side_menu_child(title, icon, child):
    # child is a two dimensioned array with id and title of menu page

    r = """<li>
            <a><i class="fa """ + icon + """\"></i> """ + title + """ <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">"""

    for row in child:
        for collumn in row:
            r += """<li>
                        <a href="/py_content/page.py?id=""" + collumn[0] + """\">""" + collumn[1] + """</a>
                    </li>"""

    r += """</ul>
          </li>"""

    return r


def media_thumb(img_url, img_id, caption=''):
    r = """<div class="col-md-55">
            <div class="thumbnail">
              <div class="image view view-first">
                <img style="width: 100%; display: block;" src=\"""" + img_url + """\" alt=\"""" + caption + """\" />
                <div class="mask no-caption">
                  <p></p>
                  <div class="tools tools-bottom">
                    <a href=?id=\"""" + img_id + """\"><i class="fa fa-pencil"></i></a>
                    <a href=?id=\"""" + img_id + """\"><i class="fa fa-times"></i></a>
                  </div>
                </div>
              </div>
              <div class="caption">
                <p>""" + caption + """</p>
              </div>
            </div>
          </div>"""

    return r


def table(data, content):
    # content is a html code with <tr> and <td> elements
    # the table auto create de search, page selector and entries selectors

    r = """<table id="datatable" class="table table-striped table-bordered">
          <thead>
            <tr>"""

    for item in data:
        r = """<th>""" + item + """</th>"""

    r += """</tr>
            </thead>
            <tbody>
                """ + content + """
            </tbody>"""

    return r


def horizontal_tab_group(tabs):
    # receive tab like two dimensioned array with title tabs and content of then
    # the ID of tab is the title of then without spaces and with lowercase.

    r = """<div class="" role="tabpanel" data-example-id="togglable-tabs">
              <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">"""
    i = 1
    for tab in tabs:
        tab_id = tab[0]

        tab_id.lower()
        tab_id.replace(' ', '-')

        r += """<li role="presentation" class>
                    <a href="#tab_content""" + str(i) + """\" id=\"""" + tab_id + """-tab" role="tab" 
                    data-toggle="tab" aria-expanded="true">
                        """ + tab[0] + """
                    </a>
                </li>"""
        i += 1

    r += """</ul>
            <div id="myTabContent" class="tab-content">"""
    i = 0
    for tab in tabs:
        tab_id = tab[0]

        tab_id.lower()
        tab_id.replace(' ', '-')

        r += """<div role="tabpanel" class="tab-pane fade active in" 
                id="tab_content""" + str(i) + """\" aria-labelledby=\"""" + tab_id + """-tab">
                """ + tab[1] + """
                </div>"""

    r += """</div>
            </div>"""

    return r


def vertical_tab_group(tabs):
    # receive tab like two dimensioned array with title tabs and content of then
    # the ID of tab is the title of then without spaces and with lowercase.

    r = """<div class="col-xs-3">
          <!-- required for floating -->
          <!-- Nav tabs -->
          <ul class="nav nav-tabs tabs-left">"""

    for tab in tabs:
        tab_id = tab[0]

        tab_id.lower()
        tab_id.replace(' ', '-')

        r += """<li class><a href="#""" + tab_id + """\" data-toggle="tab">""" + tab[0] + """</a></li>"""

    r += """</ul>
           </div>
           <div class="col-xs-9">
            <!-- Tab panes -->
            <div class="tab-content">"""

    for tab in tabs:
        tab_id = tab[0]

        tab_id.lower()
        tab_id.replace(' ', '-')

        r += """<div class="tab-pane " id=\"""" + tab_id + """\">
                    """ + tab[1] + """
                </div>"""

    r += """</div>
            </div>"""

    return r


def accordion_group(accordion):
    # receive two dimensioned array with title and content per row.

    r = """<div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">"""
    i = 1

    for row in accordion:
        r += """<div class="panel">
                    <a class="panel-heading" role="tab" id="heading""" + str(i) + """\" data-toggle="collapse" 
                    data-parent="#accordion" href="#collapse""" + str(i) + """\" aria-expanded="true" 
                    aria-controls="collapse""" + str(i) + """\">
                      <h4 class="panel-title">""" + row[0] + """</h4>
                    </a>
                    <div id="collapse""" + str(i) + """\" class="panel-collapse collapse in" role="tabpanel" 
                    aria-labelledby="heading""" + str(i) + """\">
                      <div class="panel-body">
                      """ + row[1] + """
                      </div>
                    </div>
                  </div>"""
        i += 1

    r += """</div>"""

    return r


def modal_small(title, content, modal_class):
    # use on button data-target=".model_class" to show or hide the modal
    # send a html text content to content

    r = """<div class="modal fade """ + modal_class + """\" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm">
              <div class="modal-content">

                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                  </button>
                  <h4 class="modal-title" id="myModalLabel2">""" + title + """</h4>
                </div>
                <div class="modal-body">
                  """ + content + """
                </div>
                <div class="modal-footer">
                  """ + button_common('Fechar', 'data-dismiss="modal"', 'primary') + """
                </div>

              </div>
            </div>
          </div>"""

    return r


def modal_large(title, content, modal_class):
    # use on button data-target=".model_class" to show or hide the modal
    # send a html text content to content

    r = """<div class="modal fade """ + modal_class + """\" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">

                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                  </button>
                  <h4 class="modal-title" id="myModalLabel">""" + title + """</h4>
                </div>
                <div class="modal-body">
                  """ + content + """
                </div>
                <div class="modal-footer">
                  """ + button_common('Fechar', 'data-dismiss="modal"', 'primary') + """
                </div>

              </div>
            </div>
          </div>"""

    return r


def checkbox_notification(label, is_checked=''):
    # add 'checked' if is checked for default or with some action

    r = """<p><input type="checkbox" class="flat\" """ + is_checked + """>""" + label + """</p>"""

    return r


def image_notification(title, information, mensage, img_url, notification_url):
    r = """<a href=\"""" + notification_url + """\">
            <span class="image"><img src=\"""" + img_url + """\" alt="Notification Image" /></span>
            <span>
              <span>""" + title + """</span>
              <span class="time">""" + information + """</span>
            </span>
            <span class="message">
              """ + mensage + """
            </span>
          </a>"""

    return r


def horizontal_progress_bar(progress='0', color='info'):
    # types: primary success info warning danger dark link
    # you can create you colors and works here

    r = """<div class="progress  progress-striped vertical progress_wide bottom">
            <div class="progress-bar progress-bar-""" + color + """\" 
            data-transitiongoal=\"""" + progress + """\" aria-valuenow=\"""" + progress + """\" 
            style=\"height: """ + progress + """%;"></div>
          </div>"""

    return r


def vertical_progress_bar(progress='0', color='info'):
    # types: primary success info warning danger dark link
    # you can create you colors and works here

    r = """<div class="progress">
            <div class="progress-bar progress-bar-""" + color + """\" 
            data-transitiongoal=\"""" + progress + """\" aria-valuenow=\"""" + progress + """\" 
            style=\"width: """ + progress + """%;"></div>
          </div>"""

    return r


def input_editor(label, placeholder, input_id, span=''):
    # for create input to show or give some information from image editor

    r = """<div class="input-group input-group-sm">
              <label class="input-group-addon" for=\"""" + input_id + """\">""" + label + """</label>
              <input type="text" class="form-control" id=\"""" + input_id + """\" 
              placeholder=\"""" + placeholder + """\">
              """
    if span != '':
        r += """<span class="input-group-addon">""" + span + """</span>"""

    r += """</div>"""

    return r


def button_editor(label, icon, method, option, color='default'):
    # buttons for actions of editor, move, crop, get data and some actions
    # use font awesome icons to variable icon
    # for example: fa-edit fa-play fa-save fa-users
    # types: primary success info warning danger dark link
    # you can create you colors and works here

    r = """<button type="button" class="btn btn-""" + color + """\" data-method=\"""" + method + """\" 
            data-option=\"""" + option + """\" title=\"""" + label + """"\">
              <span class="docs-tooltip" data-toggle="tooltip">
                <span class="fa """ + icon + """\"></span>
              </span>
            </button>"""

    return r


def label_editor(label, title, value, input_id):
    r = """<label class="btn btn-primary active">
              <input type="radio" class="sr-only" id=\"""" + input_id + """\" 
              name=\"aspectRatio\" value=\"""" + value + """\">
              <span class="docs-tooltip" data-toggle="tooltip" title=\"""" + title + """\">
                """ + label + """
              </span>
            </label>"""

    return r


def image_editor(img_url):
    r = """<div class="container cropper">
              <div class="row">
                <div class="col-md-9">
                  <div class="img-container">
                    <img id="image" src=\"""" + img_url + """\" alt="Picture">
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="docs-preview clearfix">
                    <div class="img-preview preview-lg"></div>
                    <div class="img-preview preview-md"></div>
                    <div class="img-preview preview-sm"></div>
                    <div class="img-preview preview-xs"></div>
                  </div>
                  <div class="docs-data">""" + \
                    input_editor('X', 'x', 'dataX', 'px') + \
                    input_editor('Y', 'y', 'dataY', 'px') + \
                    input_editor('Width', 'width', 'dataWidth', 'px') + \
                    input_editor('Height', 'height', 'dataHeight', 'px') + \
                    input_editor('Rotate', 'rotate', 'dataRotate', 'deg') + \
                    input_editor('ScaleX', 'scaleX', 'dataScaleX') + \
                    input_editor('ScaleY', 'scaleY', 'dataScaleY') + """
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-9 docs-buttons">
                  <!-- <h3 class="page-header">Toolbar:</h3> -->
                  <div class="btn-group">""" + \
                    button_editor('Move', 'fa-arrows', 'setDragMode', 'move', 'primary') + \
                    button_editor('Crop', 'fa-crop', 'setDragMode', 'crop', 'primary') + """
                  </div>
                  <div class="btn-group">""" + \
                    button_editor('Zoom In', 'fa-search-plus', '0.1', 'zoom', 'primary') + \
                    button_editor('Zoom Out', 'fa-search-minus', '-0.1', 'zoom', 'primary') + """
                  </div>
                  <div class="btn-group">""" + \
                    button_editor('Move Left', 'fa-arrow-left', '-10', 'move', 'primary') + \
                    button_editor('Move Right', 'fa-arrow-right', '10', 'move', 'primary') + \
                    button_editor('Move Up', 'fa-arrow-up', '0', 'move', 'primary') + \
                    button_editor('Move Down', 'fa-arrow-down', '0', 'move', 'primary') + """
                  </div>
                  <div class="btn-group">""" + \
                    button_editor('Rotate Left', 'fa-rotate-left', '-45', 'rotate', 'primary') + \
                    button_editor('Rotate Right', 'fa-rotate-right', '45', 'rotate', 'primary') + """
                  </div>
                  <div class="btn-group">""" + \
                    button_editor('Flip Horizontal', 'fa-arrows-h', '-1', 'scaleX', 'primary') + \
                    button_editor('Flip Vertical', 'fa-arrows-v', '-1', 'scaleY', 'primary') + """
                  </div>
                  <div class="btn-group">""" + \
                    button_editor('Crop', 'fa-check', '', 'crop', 'primary') + \
                    button_editor('Clear', 'fa-remove', '', 'clear', 'primary') + """
                  </div>
                  <div class="btn-group">""" + \
                    button_editor('Disable', 'fa-lock', '', 'disable', 'primary') + \
                    button_editor('Enable', 'fa-unlock', '', 'enable', 'primary') + """
                  </div>
                  <div class="btn-group">""" + \
                    button_editor('Reset', 'fa-refresh', '', 'reset', 'primary') + """
                    <label class="btn btn-primary btn-upload" for="inputImage" title="Upload image file">
                      <input type="file" class="sr-only" id="inputImage" name="file" accept="image/*">
                      <span class="docs-tooltip" data-toggle="tooltip" title="Import image with Blob URLs">
                        <span class="fa fa-upload"></span>
                      </span>
                    </label>
                  </div>
                  <div class="btn-group btn-group-crop">""" + \
                    button_editor('Get Cropped Canvas', '', '', 'getCroppedCanvas', 'primary') + \
                    button_editor('160&times;90', '', '{ &quot;width&quot;: 160, &quot;height&quot;: 90 }',
                                  'getCroppedCanvas', 'primary') + \
                    button_editor('320&times;180', '', '{ &quot;width&quot;: 320, &quot;height&quot;: 180 }',
                                  'getCroppedCanvas', 'primary') + """
                  </div>
                  <!-- Show the cropped image in modal -->
                  <div class="modal fade docs-cropped" id="getCroppedCanvasModal" aria-hidden="true" 
                  aria-labelledby="getCroppedCanvasTitle" role="dialog" tabindex="-1">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                          <h4 class="modal-title" id="getCroppedCanvasTitle">Cropped</h4>
                        </div>
                        <div class="modal-body"></div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <a class="btn btn-primary" id="download" href="javascript:void(0);" download="cropped.png">
                            Download
                          </a>
                        </div>
                      </div>
                    </div>
                  </div><!-- /.modal -->
                </div><!-- /.docs-buttons -->

                <div class="col-md-3 docs-toggles">
                  <!-- <h3 class="page-header">Toggles:</h3> -->
                  <div class="btn-group btn-group-justified" data-toggle="buttons">""" + \
                    label_editor('16:9', 'aspectRatio: 16 / 9', '1.7777777777777777', 'aspectRatio0') + \
                    label_editor('4:3', 'aspectRatio: 4 / 3', '1.3333333333333333', 'aspectRatio1') + \
                    label_editor('1:1', 'aspectRatio: 1 / 1', '1', 'aspectRatio2') + \
                    label_editor(' 2:3', 'aspectRatio: 2 / 3', '0.6666666666666666', 'aspectRatio3') + \
                    label_editor('Free', 'aspectRatio: NaN', 'NaN', 'aspectRatio') + """
                  </div>
                </div><!-- /.docs-toggles -->
              </div>
            </div>"""

    return r


def drop_zone(title, notice=''):
    r = """<div class="x_panel">
              <div class="x_title">
                <h2>""" + title + """</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                <p>""" + notice + """</p>
                <form action="form_upload.html" class="dropzone"></form>
                <br />
                <br />
                <br />
                <br />
              </div>
            </div>"""

    return r


# /OTHER LAYOUTS
