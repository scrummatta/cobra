# USER CONTENT LAYOUT

from py_admin.py_config.py_classes.user_content import UserData
from py_admin.py_config.py_classes.user_content import UserLogin


def get_admin_user_name(userid):
    d = UserData()
    r = d.admin_user_name(userid)

    return r


def get_admin_user_profile_img(userid):
    d = UserData()
    r = d.admin_user_img(userid)

    return r


def login_verify(username, password):
    login = UserLogin()
    r = login.check_password(username, password)
    return r
