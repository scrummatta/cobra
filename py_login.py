#!C:/Users/magalu/AppData/Local/Programs/Python/Python36/python.exe
# -*- coding: UTF-8 -*-

from py_admin.py_config.py_classes.conn import DB
import py_admin.py_config.py_functions.get_admin_content as adm
import py_admin.py_config.functions as func
import py_admin.py_config.py_functions.interface as i
import py_admin.py_content.py_modules.head as h
import py_admin.py_content.py_modules.footer as f

print("Content-type:text/html; ISO-8859-1")
print()

html = """<html lang='pt-br'>
            <head>
                %s
            </head>
            <body class="login">
                <div>
                  <a class="hiddenanchor" id="signup"></a>
                  <a class="hiddenanchor" id="signin"></a>
            
                  <div class="login_wrapper">
                    <div class="animate form login_form">
                      <section class="login_content">
                        %s
                          <div class="clearfix"></div>
                          <a class="reset_pass" href="#">%s</a>
                          <div class="separator">
                            <div class="clearfix"></div>
                            <br />
                            <div>
                              <h1><i class="fa fa-paw"></i>%s</h1>
                              <p>%s</p>
                            </div>
                          </div>
                      </section>
                    </div>
                  </div>
                </div>
            </body>
        </html>"""

hd = h.get_admin_head("Login", "")  # Panel HTML HEADER

login_input = i.input_icon("Login", "username", "user", True)
pass_input = i.input_icon("Password", "password", "lock", True, "password")

toggle = i.input_check_box("Remember-me", "style='float:left'")
sub_button = i.form_button_submit("Login")

content = "%s %s" % (login_input, pass_input)

form = i.form_box("Login", content, sub_button, "login_form")

pass_lost = "Lost your password?"

copy = adm.get_admin_copy()
title = adm.get_admin_page_title()

print(html % (hd, form, pass_lost, title, copy))
